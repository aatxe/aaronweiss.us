---
title: Home 
layout: home.njk
---

I'm Aaron Weiss, a language designer at [Roblox](https://www.roblox.com) working on
[Luau](https://luau-lang.org), a gradually-typed scripting language derived from [Lua](https://www.lua.org).

Previously, I was an [NSF Fellow](https://nsfgrfp.org) at
[Northeastern](https://www.khoury.northeastern.edu/) working on a formal semantics for Rust with
[Amal Ahmed](http://www.ccs.neu.edu/home/amal/).

As an undergraduate, I worked with [Arjun Guha](http://people.cs.umass.edu/~arjun/) on static
verification and update synthesis for [Puppet](https://puppet.com/) manifests.

I'm an open source software developer with over a decade of experience, mostly in
[Rust](http://www.rust-lang.org/), [Scala](http://www.scala-lang.org), and [OCaml](https://ocaml.org).
